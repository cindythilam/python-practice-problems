# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    # variable attendess
    attendees = len(attendees_list)
    # variable members
    members = len(members_list)
    # if number of attendees_list >= 0.50(members_list)
    if attendees >= 0.50 * members:
    # return True
        return True
    else:
        return False
