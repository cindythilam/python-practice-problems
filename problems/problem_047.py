# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    lower = False
    upper = False
    digit = False
    special = False
    for character in password:
        if character.isalpha():
            if character.isupper():
                upper = True
            else:
                lower = True
        elif character.isdigit():
            digit = True
        elif character == "$" or character == "!" or character == "@":
            special = True
    return (
        len(password) >= 6
        and len(password) <= 12
        and lower
        and upper
        and digit
        and special
    )
