# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.

# Write out your own pseudocode to help guide you.

def add_csv_lines(csv_lines):
    # new list
    # for each item in csv_lines
        # pieces = split comma
        # sum = 0
        # for each piece
            # value = convert piece into integer
            # add to sum
        # append sum to new list
    # return new list
    result = []
    for item in csv_lines:
        pieces = item.split(",")
        sum = 0
        for piece in pieces:
            value = int(piece)
            sum += value
        result.append(sum)
    return result
