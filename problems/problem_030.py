# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    # if list is empty
        # return None
    # if list has only 1 value
        # return None
    # largest = 1st in list
    # second_largest = 2nd in list
    # for value in values
        # if value > largest:
            # largest = value

    if len(values) <= 1:
        return None
    largest = values[0]
    second = values[0]
    for value in values:
        if value > largest:
            second = largest
            largest = value
        elif value > second:
            second = value
    return second
